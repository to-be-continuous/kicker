import {Component, Input} from '@angular/core';
import {Aggregated, Options, Preset, Variable, VarType} from '../kicker';

@Component({
  selector: 'app-variable-editor',
  templateUrl: './variable-editor.component.html',
  styleUrls: ['./variable-editor.component.css']
})
export class VariableEditorComponent {

  @Input() id: string;
  @Input() variable: Variable;
  @Input() aggregated: Aggregated;
  @Input() options: Options;

  get isSelect(): boolean {
    return this.variable.type === VarType.enum;
  }

  get isCheckbox(): boolean {
    return this.variable.type === VarType.boolean;
  }

  get isText(): boolean {
    return this.variable.type !== VarType.enum && this.variable.type !== VarType.boolean;
  }

  /**
   * Returns enabled presets (if part of an extension) providing a value for this variable
   */
  get presets(): Preset[] {
    return this.aggregated.getPresets(this.variable.name).filter(p => this.options.hasExtension(p.extension_id));
  }

  get hasPresets(): boolean {
    return this.presets.length > 0;
  }

  get hasResetExt(): boolean {
    return this.variable.type !== VarType.boolean;
  }
}
