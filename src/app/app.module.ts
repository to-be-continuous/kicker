import { BrowserModule } from '@angular/platform-browser';
import { NgModule, SecurityContext } from '@angular/core';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MARKED_OPTIONS, MarkdownModule, MarkedOptions, MarkedRenderer } from 'ngx-markdown';
// import {MatomoModule} from 'ngx-matomo';

import { AppComponent } from './app.component';
import { KickerComponent } from './kicker/kicker.component';
import { GitlabCiComponent } from './gitlab-ci/gitlab-ci.component';
import { TemplatesSectionComponent } from './templates-section/templates-section.component';
import { StepbarComponent } from './stepbar/stepbar.component';
import { VariableEditorComponent } from './variable-editor/variable-editor.component';
import { OptionsBoxComponent } from './options-box/options-box.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OptionsFormComponent } from './options-form/options-form.component';

// Override `MarkedOptions` renderer to open in a new tab for links
export function markedOptionsFactory(): MarkedOptions {
    const renderer = new MarkedRenderer();
    const linkRenderer = renderer.link;

    renderer.link = (href, title, text) => {
        const html = linkRenderer.call(renderer, href, title, text);
        return html.replace(/^<a /, '<a target="_blank" rel="nofollow noopener noreferrer" ');
    };

    return {
        renderer: renderer,
        gfm: true,
        breaks: false,
        pedantic: false,
    };
}

@NgModule({
    declarations: [
        AppComponent,
        KickerComponent,
        GitlabCiComponent,
        TemplatesSectionComponent,
        StepbarComponent,
        VariableEditorComponent,
        OptionsBoxComponent,
        OptionsFormComponent,
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        NgbModule,
        MarkdownModule.forRoot({
            sanitize: SecurityContext.NONE,
            markedOptions: {
                provide: MARKED_OPTIONS,
                useFactory: markedOptionsFactory,
            },
        }),
    ],
    providers: [provideHttpClient(withInterceptorsFromDi())],
})
export class AppModule {}
