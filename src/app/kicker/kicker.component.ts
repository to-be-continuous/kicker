import {Component, OnInit} from '@angular/core';
import {Aggregated, Options, Template} from '../kicker';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-kicker',
  templateUrl: './kicker.component.html',
  styleUrls: ['./kicker.component.css']
})
export class KickerComponent implements OnInit {

  aggregated: Aggregated;
  templates: Template[] = [];
  options = new Options();
  serverHost = environment.envVar.CI_SERVER_HOST || 'gitlab.com';

  constructor(
    private http: HttpClient,
  ) {
  }

  ngOnInit(): void {
    this.http.get<Template[]>(environment.kickermodel.url)
      .subscribe(value => {
        this.aggregated = Aggregated.fromJson(value);
        this.templates = this.aggregated.templates;
        console.log(`templates loaded from ${environment.kickermodel.url}:`, this.aggregated);
      });
  }

  /**
   * Return templates of given kind, part of selected extensions
   * @param kind template kind to filter
   * @param addNone whether to add a 'none' template entry
   */
  templatesOfKind(kind: string, addNone?: boolean): Template[] {
    const templates = this.templates.filter(t => t.kind === kind && this.options.hasExtension(t.extension_id));
    if (addNone) {
      templates.splice(0, 0, new Template('none', 'none...', 'No template selected.'));
    }
    return templates;
  }

  templatesNotOfKind(kinds: string[]): Template[] {
    return this.templates.filter(t => kinds.indexOf(t.kind) < 0 && this.options.hasExtension(t.extension_id));
  }
}
