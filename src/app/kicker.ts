export enum VarType {
  text = "text",
  url = "url",
  boolean = "boolean",
  enum = "enum",
  number = "number"
}

export class Project {

  constructor(name: string, tag: string, tags: string[], path: string, web_url: string, avatar: string) {
    this.name = name;
    this.path = path;
    this.tag = tag;
    this.tags = tags || [tag];
    this.web_url = web_url;
    this.avatar = avatar;
  }

  name: string;
  tag: string;
  tags: string[];
  path: string;
  web_url: string;
  avatar: string;

  getLatestVersion(mode: VersionMode): string {
    const latestFull = this.tags[0];
    switch(mode) {
      case VersionMode.major: {
        return latestFull.substring(0, latestFull.indexOf('.'));
      }
      case VersionMode.minor: {
        return latestFull.substring(0, latestFull.lastIndexOf('.'));
      }
      default: {
        return latestFull;
      }
    }
  }

  static fromJson(data: any): Project {
    return data ? new Project(data.name, data.tag, data.tags, data.path, data.web_url, data.avatar) : null;
  }
}

export class Variable {

  constructor(name: string, description: string, dflt: string, type: VarType = VarType.text,
              mandatory: boolean, secret: boolean, advanced: boolean, values: string[]) {
    this.name = name;
    this.description = description;
    this.dflt = dflt;
    this.type = type;
    this.mandatory = mandatory;
    this.secret = secret;
    this.advanced = advanced;
    this.values = values;
  }

  template: Template;
  feature: Feature;
  name: string;
  description: string;
  dflt: string;
  type: VarType = VarType.text;
  mandatory: boolean;
  secret: boolean;
  advanced: boolean;
  values: string[];

  // editor
  val: any;

  /**
   * Default getter (implementing selected variants)
   */
  get default(): string {
    if (this.template !== undefined) {
      if (this.feature !== undefined) {
        // this is a feature variable
        const overridden = this.template.variants
          // filter enabled variants
          .filter(variant => variant.enabled)
          // map to overridden feature / variable (or undefined)
          .map(variant => variant.getFeature(this.feature.id)?.getVariable(this.name))
          // filter out undefined
          .filter(v => v !== undefined)
          // pick first
          .pop();
        return overridden ? overridden.dflt : this.dflt;
      } else {
        // this is a template global variable
        const overridden = this.template.variants
          // filter enabled variants
          .filter(variant => variant.enabled)
          // map to overridden feature / variable (or undefined)
          .map(variant => variant.getVariable(this.name))
          // filter out undefined
          .filter(v => v !== undefined)
          // pick first
          .pop();
        return overridden ? overridden.dflt : this.dflt;
      }
    }
    return this.dflt;
  }

  /**
   * Value getter
   */
  get inputValue(): any {
    switch (this.type) {
      case VarType.boolean:
      case VarType.number:
          return this.value;
      default:
        return '"'+this.value+'"';
    }
  }

  get value(): any {
    if (this.val !== undefined) {
      return this.val;
    }
    switch (this.type) {
      case VarType.boolean:
        return this.default === 'true';
      default:
        return this.default === undefined ? '' : this.default;
    }
  }

  /**
   * Value setter
   */
  set value(val: any) {
    if (this.type === VarType.boolean && val === false) {
      val = undefined;
    }
    this.val = val;
  }

  /**
   * Determines whether the variable has default value
   */
  get isDefault(): boolean {
    return this.val === undefined || this.val === this.default;
  }

  static fromJson(data: any): Variable {
    return new Variable(data.name, data.description, data.default, VarType[data.type],
      data.mandatory, data.secret, data.advanced, data.values);
  }

  /**
   * Resets the variable value
   */
  reset(): void {
    delete this.val;
  }


}

export class Feature {

  constructor(id: string, name: string, description: string, enable_with: string, disable_with: string, variables: Variable[]) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.enable_with = enable_with;
    this.disable_with = disable_with;
    this.variables = variables;
    // a feature is enabled by default when disable_with is set
    this.enabled = disable_with !== undefined;
  }

  id: string;
  name: string;
  description: string;
  enable_with: string;
  disable_with: string;
  variables: Variable[] = [];

  // editor
  enabled: boolean;

  get secrets(): Variable[] {
    return this.variables.filter(value => value.secret);
  }

  get nonSecrets(): Variable[] {
    return this.variables.filter(value => !value.secret);
  }

  static fromJson(data: any): Feature {
    return new Feature(data.id, data.name, data.description, data.enable_with, data.disable_with,
      data.variables ? data.variables.map(v => Variable.fromJson(v)) : []);
  }

  getVariable(name: string): Variable | undefined {
    return this.variables.find(value => value.name === name);
  }
}

export class Variant {

  constructor(id: string, name: string, description: string, template_path: string,
              extension_id: string, project: Project, variables: Variable[], features: Feature[]) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.template_path = template_path;
    this.project = project;
    this.variables = variables;
    this.features = features;
  }

  id: string;
  name: string;
  description: string;
  template_path: string;
  extension_id: string;
  project: Project;
  variables: Variable[] = [];
  features: Feature[] = [];

  // editor
  enabled = false;

  static fromJson(data: any, defaultProject: Project): Variant {
    return new Variant(data.id, data.name, data.description, data.template_path, data.extension_id,
      data.project ? Project.fromJson(data.project) : defaultProject,
      data.variables ? data.variables.map(v => Variable.fromJson(v)) : [],
      data.features ? data.features.map(v => Feature.fromJson(v)) : []);
  }

  get templateFile(): string {
    return this.template_path.split(/[/.]/)[1]
  }

  getVariable(name: string): Variable | undefined {
    return this.variables.find(value => value.name === name);
  }

  getFeature(id: string): Feature | undefined {
    return this.features.find(value => value.id === id);
  }
}

export class Template {

  constructor(id: string, name: string, description: string, template_path?: string, prefix?: string,
              project?: Project, kind?: string, extension_id?: string,
              variables: Variable[] = [], features: Feature[] = [], variants: Variant[] = []) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.template_path = template_path;
    this.prefix = prefix;
    this.kind = kind;
    this.project = project;
    this.extension_id = extension_id;
    this.variables = variables;
    this.features = features;
    this.variants = variants;
  }

  get secrets(): Variable[] {
    return this.variables.filter(value => value.secret);
  }

  get nonSecrets(): Variable[] {
    return this.variables.filter(value => !value.secret);
  }

  get templateFile(): string {
    return this.template_path.split(/[/.]/)[1]
  }

  get var_prefix(): string {
    if (!this.prefix) {
      // guess from variables: pick prefix from first variable ending with "_IMAGE"
      const img_vars = this.variables.filter(variable => variable.name.endsWith("_IMAGE"))
      if (img_vars.length > 0) {
        this.prefix=img_vars[0].name.substring(0, img_vars[0].name.indexOf('_'))
      } else {
        // otherwise use template name (uppercase)
        this.prefix=this.name.toUpperCase()
      }
    }
    return this.prefix
  }

  getInputName(var_name: string): string {
    // 1: strip prefix
    let unprefixed_var_name=var_name;
    if (unprefixed_var_name.startsWith(this.var_prefix+'_')) {
      unprefixed_var_name=unprefixed_var_name.substring(this.var_prefix.length+1)
    }
    // 2: lowercase and hyphens
    return unprefixed_var_name.toLowerCase().replace(/_/g, '-');
  }

  id: string;
  name: string;
  description: string;
  template_path: string;
  kind: string;
  project: Project;
  extension_id: string;
  variables: Variable[];
  features: Feature[];
  variants: Variant[];
  prefix?: string;

  // editor
  enabled = false;

  static fromJson(data: any): Template {
    const project = Project.fromJson(data.project);
    const template = new Template(
      data.project.path.replace('/', '_'),
      data.name,
      data.description,
      data.template_path,
      data.prefix?.toUpperCase(),
      project,
      data.kind,
      data.extension_id,
      data.variables ? data.variables.map(v => Variable.fromJson(v)) : [],
      data.features ? data.features.map(v => Feature.fromJson(v)) : [],
      data.variants ? data.variants.map(v => Variant.fromJson(v, project)) : []
    );
    // parenting
    template.variables.forEach(variable => variable.template = template);
    template.features.forEach(feature => {
      feature.variables.forEach(variable => {
        variable.template = template;
        variable.feature = feature;
      });
    });
    return template;
  }
}

export class Preset {

  constructor(name: string, description: string, extension_id: string, values: object) {
    this.name = name;
    this.description = description;
    this.extension_id = extension_id;
    this.values = values;
  }

  name: string;
  description: string;
  extension_id: string;
  values: object;

  // editor
  enabled = false;

  static fromJson(data: any): Preset {
    return new Preset(
      data.name,
      data.description,
      data.extension_id,
      data.values
    );
  }

  hasVariable(variable: string): boolean {
    return this.values[variable] !== undefined;
  }

  getValue(variable: string): string {
    return this.values[variable];
  }

}
export class Extension {

  constructor(id: string, name: string, description: string) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  id: string;
  name: string;
  description: string;

  // editor
  enabled = false;

  static fromJson(data: any): Extension {
    return new Extension(data.id, data.name, data.description);
  }
}

export class Aggregated {

  constructor(extensions: Extension[], templates: Template[], presets: Preset[]) {
    this.extensions = extensions;
    this.templates = templates;
    this.presets = presets;
  }

  extensions: Extension[];
  templates: Template[];
  presets: Preset[];

  static fromJson(data: any): Aggregated {
    return new Aggregated(
      data.extensions.map(v => Extension.fromJson(v)),
      data.templates.map(v => Template.fromJson(v)),
      data.presets.map(v => Preset.fromJson(v)),
    );
  }

  getExtension(id: string): Extension | undefined {
    return this.extensions.find(value => value.id === id);
  }

  getPresets(variable: string): Preset[] {
    return this.presets.filter(p => p.hasVariable(variable));
  }

  applyPreset(preset: Preset) {
    this.templates.forEach(tmpl => {
      tmpl.variables.filter(variable => preset.hasVariable(variable.name)).forEach(v => v.value = preset.getValue(v.name));
      tmpl.features.forEach(feature => {
        feature.variables.filter(variable => preset.hasVariable(variable.name)).forEach(v => v.value = preset.getValue(v.name));
      });
    });
  }
}

export enum IncludeMode {
  project = "project",
  component = "component",
  remote = "remote",
}

export enum VersionMode {
  major = "major",
  minor = "minor",
  full = "full",
}

export class Options {
  showAdvanced = false;
  showStages = false;
  includeMode = IncludeMode.component;
  versionMode = VersionMode.minor;
  ext2enabled = new Map<string, boolean>();

  hasExtension(id?: string): boolean {
    return id == null || this.ext2enabled.get(id);
  }

  setExtension(id: string, enabled: boolean) {
    this.ext2enabled.set(id, enabled);
  }
}
