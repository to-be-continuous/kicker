import {Component, ElementRef, Input} from '@angular/core';
import {IncludeMode, Options, Template, VersionMode} from '../kicker';
// import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-gitlab-ci',
  templateUrl: './gitlab-ci.component.html',
  styleUrls: ['./gitlab-ci.component.css']
})
export class GitlabCiComponent {
  @Input() templates: Template[];
  // @ViewChild('#copy-yaml-to-clipboard') copyButtonRef: ElementRef;
  // @ViewChild('#gitlab-ci-content') yamlContent: ElementRef;
  @Input() options: Options;
  @Input() serverHost: string;
  keys = Object.keys;
  includeModes = IncludeMode;
  versionModes = VersionMode

  constructor(
    private hostRef: ElementRef // ,
    // private matomoTracker: MatomoTracker
  ) {
  }

  isProjectMode() {
    return this.options.includeMode == IncludeMode.project;
  }
  
  hasIncludes() {
    return this.templates.filter(t => t.enabled).length > 0;
  }

  hasSecrets() {
    for (const template of this.templates) {
      if (template.enabled) {
        if (template.secrets.length > 0) {
          return true;
        }
        for (const feature of template.features) {
          if (feature.enabled) {
            if (feature.secrets.length > 0) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  /**
   * Determines whether there is at least one variable set (not default)
   */
  hasVariables() {
    if (this.options.includeMode == IncludeMode.component) {
      return false;
    }
    for (const template of this.templates) {
      if (template.enabled) {
        if (template.variables.filter(variable => !variable.secret && !variable.isDefault).length > 0) {
          return true;
        }
        for (const feature of template.features) {
          if (feature.enabled) {
            if (feature.enable_with) {
              return true;
            }
            if (feature.variables.filter(variable => !variable.secret && !variable.isDefault).length > 0) {
              return true;
            }
          } else {
            if (feature.disable_with) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  hasInputs(template: any) {
    if (template.variables.filter(variable => !variable.secret && !variable.isDefault).length > 0) {
      return true;
    }
    for (const feature of template.features) {
      if (feature.enabled) {
        if (feature.enable_with) {
          return true;
        }
        if (feature.variables.filter(variable => !variable.secret && !variable.isDefault).length > 0) {
          return true;
        }
      } else {
        if (feature.disable_with) {
          return true;
        }
      }
    }
  }

  copyToClipboard() {
    // select
    const node = document.getElementById('gitlab-ci-content');
    // copy to clipboard
    navigator.clipboard.writeText(node.innerText);
    // Matomo tracking
    // this.matomoTracker.trackEvent('kicker', 'copy-yaml');
  }
}
