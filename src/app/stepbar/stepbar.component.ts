import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-stepbar',
  templateUrl: './stepbar.component.html',
  styleUrls: ['./stepbar.component.css']
})
export class StepbarComponent {

  @HostListener('window:scroll')
  onWindowScroll() {
    const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    const stepbar = document.getElementById('stepbar');
    // const barHeight = stepbar.offsetHeight + 25;
    const barHeight = 65;
    const items = stepbar.getElementsByClassName('stepped-process-item');
    let found = false;
    for (let i = items.length - 1; i >= 0; i--) {
      const item = items.item(i);
      if (found) {
        item.setAttribute('class', 'stepped-process-item done');
      } else {
        const link = item.getElementsByClassName('stepped-process-link').item(0);
        const target = document.getElementById(link.getAttribute('href').substr(1));
        if (target && scrollTop + barHeight >= target.offsetTop) {
          item.setAttribute('class', 'stepped-process-item active');
          found = true;
        } else {
          item.setAttribute('class', 'stepped-process-item next');
        }
      }
    }
  }

}
