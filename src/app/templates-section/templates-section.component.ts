import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Aggregated, Options, Template, Variant } from '../kicker';
import { MarkdownService } from 'ngx-markdown';
// import { MatomoTracker } from 'ngx-matomo';

@Component({
    selector: 'app-templates-section',
    templateUrl: './templates-section.component.html',
    styleUrls: ['./templates-section.component.css'],
})
export class TemplatesSectionComponent {
    /**
     * Section ID
     */
    @Input() id: string;
    /**
     * Section title
     */
    @Input() sectionTitle: string;
    /**
     * Aggregated model
     */
    @Input() aggregated: Aggregated;
    /**
     * Templates in this section
     */
    @Input() templates: Template[];
    /**
     * Determines whether this section allows multiple or single template selection
     */
    @Input() multiple: boolean;
    /**
     * Kicker options
     */
    @Input() options: Options;

    constructor(private markdownService: MarkdownService) {}

    // constructor(
    //   private matomoTracker: MatomoTracker
    // ) {
    // }

    /**
     * Returns the selected template ID (for non-multiple section only)
     */
    get selected(): string {
        for (const template of this.templates) {
            if (template.enabled) {
                return template.id;
            }
        }
        // if none found: select first
        this.templates[0].enabled = true;
        return this.templates[0].id;
    }

    /**
     * Sets the selected template ID (for non-multiple section only)
     */
    set selected(id: string) {
        for (const template of this.templates) {
            template.enabled = template.id === id;
        }
    }

    trackEvent(action: string, name?: string) {
        console.log('event', action, name);
        // this.matomoTracker.trackEvent('kicker', action, name);
    }

    renderMarkdown(template: string) {
        return this.markdownService.parse(template);
    }

    /**
     * Return template variants, part of selected extensions
     */
    variantsFor(template: Template): Variant[] {
        return template.variants.filter((v) => this.options.hasExtension(v.extension_id));
    }
}
