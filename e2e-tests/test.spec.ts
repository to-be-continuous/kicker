import { test, expect } from '@playwright/test';

test('Has title and simple select', async ({ page }) => {
    await page.goto('/');
    await expect(page).toHaveTitle(/to be continuous - Kicker/);

    const buildSelector = page.locator('#build-choice');
    await buildSelector.selectOption({ value: 'to-be-continuous_angular' });

    await page.waitForTimeout(3000);
    await expect(page.locator('body')).toContainText('Angular lint analysis');
});
